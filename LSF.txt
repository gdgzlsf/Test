自创说明,#genre#

LSF极速自创版,http://58.20.202.43:808/hls/100/index.m3u8

更新于20250102,http://58.20.202.43:808/hls/101/index.m3u8

且用且珍惜,http://58.20.202.43:808/hls/102/index.m3u8

CCTV1,http://58.19.38.162:9901/tsfile/live/1000_1.m3u8

CCTV2,http://58.19.38.162:9901/tsfile/live/1001_1.m3u8

CCTV3,http://58.19.38.162:9901/tsfile/live/1002_1.m3u8

CCTV4,http://58.19.38.162:9901/tsfile/live/1003_1.m3u8

CCTV5,http://58.19.38.162:9901/tsfile/live/1004_1.m3u8

CCTV5+,http://58.19.38.162:9901/tsfile/live/1014_1.m3u8

CCTV6,http://58.19.38.162:9901/tsfile/live/1005_1.m3u8

CCTV7,http://58.19.38.162:9901/tsfile/live/1006_1.m3u8

CCTV8,http://58.19.38.162:9901/tsfile/live/1007_1.m3u8

CCTV9,http://58.19.38.162:9901/tsfile/live/1008_1.m3u8

CCTV10,http://58.19.38.162:9901/tsfile/live/1009_1.m3u8

CCTV11,http://58.19.38.162:9901/tsfile/live/1010_1.m3u8

CCTV12,http://58.19.38.162:9901/tsfile/live/1011_1.m3u8

CCTV13,http://58.19.38.162:9901/tsfile/live/1012_1.m3u8

CCTV14,http://58.19.38.162:9901/tsfile/live/1013_1.m3u8

CCTV11,http://58.19.38.162:9901/tsfile/live/1010_1.m3u8

CCTV12,http://58.19.38.162:9901/tsfile/live/1011_1.m3u8

CCTV13,http://58.19.38.162:9901/tsfile/live/1012_1.m3u8

CCTV14,http://58.19.38.162:9901/tsfile/live/1013_1.m3u8
CCTV15,http://58.19.38.162:9901/tsfile/live/1015_1.m3u8
CCTV17,http://58.19.38.162:9901/tsfile/live/1016_1.m3u8

央视[IPV4],#genre#
CCTV-1综合,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226016/index.m3u8
CCTV-2财经,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225588/index.m3u8
CCTV-3综艺,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226021/index.m3u8
CCTV-4中文国际,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226428/index.m3u8
CCTV-5体育,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226019/index.m3u8
CCTV-5+体育赛事,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225603/index.m3u8
CCTV-6电影,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226010/index.m3u8#http://121.24.98.226:8090/hls/14/index.m3u8
CCTV-7国防军事·,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225733/index.m3u8
CCTV-8电视剧,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226008/index.m3u8
CCTV-9纪录,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225734/index.m3u8
CCTV-10科教,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225730/index.m3u8
CCTV-12社会与法,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225731/index.m3u8
CCTV-13新闻,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226537/index.m3u8
CCTV-14少儿,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225732/index.m3u8
CCTV-16奥林匹克,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226100/index.m3u8
CCTV-17农业农村,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225765/index.m3u8

卫视[IPV4],#genre#
河南卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226480/index.m3u8#https://pi.0472.org/live/hnws.m3u8?token=162210
江苏卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226310/index.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225613/index.m3u8#http://121.24.98.226:8090/hls/38/index.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226495/index.m3u8
湖北卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226477/index.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225627/index.m3u8
深圳卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225739/index.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226313/index.m3u8
浙江卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226339/index.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225612/index.m3u8#http://ali-m-l.cztv.com/channels/lantian/channel001/1080p.m3u8
广西卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226380/index.m3u8
青海卫视,http://stream.qhbtv.com/qhws/sd/live.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225628/index.m3u8
广东卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226248/index.m3u8
东方卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226345/index.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225735/index.m3u8
湖南卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226307/index.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225610/index.m3u8
重庆卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226409/index.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225618/index.m3u8
北京卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225728/index.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226450/index.m3u8
黑龙江卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226327/index.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226419/index.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226016/index.m3u8#http://[2409:8087:1a01:df::4077]/ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226327/index.m3u8#https://live.v1.mk/api/bestv.php?id=hljwshd8m/8000000
辽宁卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226546/index.m3u8
安徽卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226391/index.m3u8
四川卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226338/index.m3u8
江西卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226344/index.m3u8
海南卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226465/index.m3u8
东南卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226341/index.m3u8
河北卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226406/index.m3u8
吉林卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226397/index.m3u8#http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226386/index.m3u8
新疆卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225635/index.m3u8
甘肃卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225633/index.m3u8
宁夏卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225632/index.m3u8
陕西卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225625/index.m3u8
山西卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225624/index.m3u8
贵州卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225626/index.m3u8
西藏卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225638/index.m3u8
山东卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226456/index.m3u8
兵团卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226439/index.m3u8
云南卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226444/index.m3u8
延边卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221226516/index.m3u8
内蒙古卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225634/index.m3u8
山东教育卫视,http://ottrrs.hl.chinamobile.com/PLTV/88888888/224/3221225655/index.m3u8



HK+TW,#genre#

台湾民视新闻,http://38.64.72.148:80/hls/modn/list/4012/chunklist1.m3u8
台湾公视,http://50.7.234.10:8278/ctv_taiwan/playlist.m3u8?tid=MDCD6096520560965205&ct=19249&tsum=64237c7ffa0c00b62657a938247bcab0
台湾中视,http://litv.zapi.us.kg/?id=4gtv-4gtv040
台湾八大綜合,http://61.221.215.25:8800/hls/56/index.m3u8
台湾東森新聞,http://aktv.top/AKTV/live/aktv2/null/AKTV.m3u8
台湾三立新闻,https://epg.pw/stream/bad9a4ebaf24f0b19d4edd02bef68b5dd5b463eb5527e4b44bb68e23248c6ddb.m3u8
台湾中天新闻,http://litv.zapi.us.kg/?id=4gtv-4gtv009
台湾TVBS,http://38.64.72.148:80/hls/modn/list/4005/chunklist0.m3u8
台湾寰宇新聞台,https://aktv.top/AKTV/live/aktv/null-9/AKTV.m3u8
翡翠台HD,http://php.jdshipin.com:8880/smt.php?id=jade_twn
https://aktv.top/AKTV/live/aktv/null/AKTV.m3u8
无线新闻,http://php.jdshipin.com:8880/smt.php?id=inews_twn
凤凰中文,http://pi.0472.org/tv/cbn.php?id=xfjcHD
凤凰资讯,http://php.jdshipin.com:8880/TVOD/iptv.php?id=fhzx
凤凰香港,https://epg.pw/stream/af200edb390eccb6bd2710d91f2d129e2fa16a7ec32f42733606bbd126b434c0.m3u8

AKTV,#genre#

翡翠台,http://aktv.top/AKTV/live/aktv/null/AKTV.m3u8
無線新聞台,http://aktv.top/AKTV/live/aktv/null-1/AKTV.m3u8
TVB Plus,http://aktv.top/AKTV/live/aktv/tvbplus/AKTV.m3u8
Now星影台,http://aktv.top/AKTV/live/aktv2/now/AKTV.m3u8
Now爆谷台,http://aktv.top/AKTV/live/aktv2/now-1/AKTV.m3u8
美亞電影台(HK),http://aktv.top/AKTV/live/aktv/hk-1/AKTV.m3u8
鳳凰中文,http://aktv.top/AKTV/live/aktv/null-3/AKTV.m3u8
鳳凰資訊,http://aktv.top/AKTV/live/aktv/null-4/AKTV.m3u8
鳳凰香港,http://aktv.top/AKTV/live/aktv/null-5/AKTV.m3u8
28 AI 智慧賽馬,http://aktv.top/AKTV/live/aktv/28ai/AKTV.m3u8
Channel 5 HD,http://aktv.top/AKTV/live/aktv/channel5hd/AKTV.m3u8
Channel 8 HD,http://aktv.top/AKTV/live/aktv/channel8hd/AKTV.m3u8
Channel U HD,http://aktv.top/AKTV/live/aktv/channeluhd/AKTV.m3u8
myTV SUPER 18台,http://aktv.top/AKTV/live/aktv/mytvsuper18/AKTV.m3u8
PopC,http://aktv.top/AKTV/live/aktv/popc/AKTV.m3u8
ROCK Action,http://aktv.top/AKTV/live/aktv/rockaction/AKTV.m3u8
TVBS,http://aktv.top/AKTV/live/aktv/tvbs/AKTV.m3u8
TVBS-新聞台,http://aktv.top/AKTV/live/aktv/tvbs-1/AKTV.m3u8
tvN,http://aktv.top/AKTV/live/aktv/tvn/AKTV.m3u8
中天亞洲台,http://aktv.top/AKTV/live/aktv/null-12/AKTV.m3u8
中天新聞台,http://aktv.top/AKTV/live/aktv/null-8/AKTV.m3u8
中視,http://aktv.top/AKTV/live/aktv/null-10/AKTV.m3u8
千禧經典台,http://aktv.top/AKTV/live/aktv/null-15/AKTV.m3u8
娛樂新聞台(HK),http://aktv.top/AKTV/live/aktv/hk/AKTV.m3u8
寰宇新聞台,http://aktv.top/AKTV/live/aktv/null-9/AKTV.m3u8
日本全天新聞,http://aktv.top/AKTV/live/aktv/null-13/AKTV.m3u8
明珠剧集台(北美),http://aktv.top/AKTV/live/aktv/null-19/AKTV.m3u8
明珠台,http://aktv.top/AKTV/live/aktv/null-2/AKTV.m3u8
翡翠剧集台(北美),http://aktv.top/AKTV/live/aktv/null-18/AKTV.m3u8
翡翠综合台(北美),http://aktv.top/AKTV/live/aktv/null-17/AKTV.m3u8
華視,http://aktv.top/AKTV/live/aktv/null-11/AKTV.m3u8
靖天電影台,http://aktv.top/AKTV/live/aktv/null-6/AKTV.m3u8
黃金翡翠台,http://aktv.top/AKTV/live/aktv/null-21/AKTV.m3u8
龍華日韓台,http://aktv.top/AKTV/live/aktv/null-22/AKTV.m3u8
龍華經典台,http://aktv.top/AKTV/live/aktv/null-7/AKTV.m3u8
龍華電影台,http://aktv.top/AKTV/live/aktv/null-23/AKTV.m3u8
i-Fun 動漫台,http://aktv.top/AKTV/live/aktv2/ifun/AKTV.m3u8
viutv 99,http://aktv.top/AKTV/live/aktv2/viutv99/AKTV.m3u8
viutv6 96,http://aktv.top/AKTV/live/aktv2/viutv696/AKTV.m3u8
无线新闻台(北美),http://aktv.top/AKTV/live/aktv2/null-1/AKTV.m3u8
東森新聞,http://aktv.top/AKTV/live/aktv2/null/AKTV.m3u8
Animax-HK,http://aktv.top/AKTV/live/aktv3/animaxhk/AKTV.m3u8
DW,http://aktv.top/AKTV/live/aktv3/dw/AKTV.m3u8
France 24,http://aktv.top/AKTV/live/aktv3/france24/AKTV.m3u8
KIX,http://aktv.top/AKTV/live/aktv3/kix/AKTV.m3u8
Love Nature HD,http://aktv.top/AKTV/live/aktv3/lovenaturehd/AKTV.m3u8
NHK World-Japan,http://aktv.top/AKTV/live/aktv3/nhkworldjapan/AKTV.m3u8
Nick Jr,http://aktv.top/AKTV/live/aktv3/nickjr/AKTV.m3u8
Nickelodeon,http://aktv.top/AKTV/live/aktv3/nickelodeon/AKTV.m3u8
ROCK Action,http://aktv.top/AKTV/live/aktv3/rockaction/AKTV.m3u8
Thrill,http://aktv.top/AKTV/live/aktv3/thrill/AKTV.m3u8
ZooMoo,http://aktv.top/AKTV/live/aktv3/zoomoo/AKTV.m3u8
亞洲新聞台,http://aktv.top/AKTV/live/aktv3/null-3/AKTV.m3u8
東方衛視國際頻道,http://aktv.top/AKTV/live/aktv3/null/AKTV.m3u8
無線衛星新聞台,http://aktv.top/AKTV/live/aktv3/null-2/AKTV.m3u8
28 AI 智慧賽馬,http://aktv.top/AKTV/live/aktv/28ai/AKTV.m3u8
Channel 5 HD,http://aktv.top/AKTV/live/aktv/channel5hd/AKTV.m3u8
Channel 8 HD,http://aktv.top/AKTV/live/aktv/channel8hd/AKTV.m3u8
Channel U HD,http://aktv.top/AKTV/live/aktv/channeluhd/AKTV.m3u8
myTV SUPER 18台,http://aktv.top/AKTV/live/aktv/mytvsuper18/AKTV.m3u8
PopC,http://aktv.top/AKTV/live/aktv/popc/AKTV.m3u8
ROCK Action,http://aktv.top/AKTV/live/aktv/rockaction/AKTV.m3u8
TVB Plus,http://aktv.top/AKTV/live/aktv/tvbplus/AKTV.m3u8
TVBS,http://aktv.top/AKTV/live/aktv/tvbs/AKTV.m3u8
TVBS-新聞台,http://aktv.top/AKTV/live/aktv/tvbs-1/AKTV.m3u8
tvN,http://aktv.top/AKTV/live/aktv/tvn/AKTV.m3u8
中天亞洲台,http://aktv.top/AKTV/live/aktv/null-12/AKTV.m3u8
中天新聞台,http://aktv.top/AKTV/live/aktv/null-8/AKTV.m3u8
中視,http://aktv.top/AKTV/live/aktv/null-10/AKTV.m3u8
千禧經典台(HK),http://aktv.top/AKTV/live/aktv/null-15/AKTV.m3u8
娛樂新聞台(HK),http://aktv.top/AKTV/live/aktv/hk/AKTV.m3u8
寰宇新聞台,http://aktv.top/AKTV/live/aktv/null-9/AKTV.m3u8
日本全天新聞,http://aktv.top/AKTV/live/aktv/null-13/AKTV.m3u8
明珠剧集台(北美),http://aktv.top/AKTV/live/aktv/null-19/AKTV.m3u8
明珠台,http://aktv.top/AKTV/live/aktv/null-2/AKTV.m3u8
無線新聞台,http://aktv.top/AKTV/live/aktv/null-1/AKTV.m3u8
美亞電影台(HK),http://aktv.top/AKTV/live/aktv/hk-1/AKTV.m3u8
翡翠剧集台(北美),http://aktv.top/AKTV/live/aktv/null-18/AKTV.m3u8
翡翠台,http://aktv.top/AKTV/live/aktv/null/AKTV.m3u8
翡翠综合台(北美),http://aktv.top/AKTV/live/aktv/null-17/AKTV.m3u8
華視,http://aktv.top/AKTV/live/aktv/null-11/AKTV.m3u8
靖天電影台,http://aktv.top/AKTV/live/aktv/null-6/AKTV.m3u8
鳳凰中文,http://aktv.top/AKTV/live/aktv/null-3/AKTV.m3u8
鳳凰資訊,http://aktv.top/AKTV/live/aktv/null-4/AKTV.m3u8
鳳凰香港,http://aktv.top/AKTV/live/aktv/null-5/AKTV.m3u8
黃金翡翠台,http://aktv.top/AKTV/live/aktv/null-21/AKTV.m3u8
龍華日韓台,http://aktv.top/AKTV/live/aktv/null-22/AKTV.m3u8
龍華經典台,http://aktv.top/AKTV/live/aktv/null-7/AKTV.m3u8
龍華電影台,http://aktv.top/AKTV/live/aktv/null-23/AKTV.m3u8
AXN,http://aktv.top/AKTV/live/aktv2/axn/AKTV.m3u8
i-Fun 動漫台,http://aktv.top/AKTV/live/aktv2/ifun/AKTV.m3u8
Love Nature HD,http://aktv.top/AKTV/live/aktv2/lovenaturehd/AKTV.m3u8
Now星影台,http://aktv.top/AKTV/live/aktv2/now/AKTV.m3u8
Now爆谷台,http://aktv.top/AKTV/live/aktv2/now-1/AKTV.m3u8
viutv 99,http://aktv.top/AKTV/live/aktv2/viutv99/AKTV.m3u8
viutv6 96,http://aktv.top/AKTV/live/aktv2/viutv696/AKTV.m3u8
无线新闻台(北美),http://aktv.top/AKTV/live/aktv2/null-1/AKTV.m3u8
東森新聞,http://aktv.top/AKTV/live/aktv2/null/AKTV.m3u8
Animax(HK),http://aktv.top/AKTV/live/aktv3/animaxhk/AKTV.m3u8
Astro AEC,http://aktv.top/AKTV/live/aktv3/astroaec/AKTV.m3u8
Astro iQIYI,http://aktv.top/AKTV/live/aktv3/astroiqiyi/AKTV.m3u8
Astro Xing He,http://aktv.top/AKTV/live/aktv3/astroxinghe/AKTV.m3u8
DW,http://aktv.top/AKTV/live/aktv3/dw/AKTV.m3u8
France 24,http://aktv.top/AKTV/live/aktv3/france24/AKTV.m3u8
KIX,http://aktv.top/AKTV/live/aktv3/kix/AKTV.m3u8
Love Nature HD,http://aktv.top/AKTV/live/aktv3/lovenaturehd/AKTV.m3u8
NHK World-Japan,http://aktv.top/AKTV/live/aktv3/nhkworldjapan/AKTV.m3u8
NHK東京総合,http://aktv.top/AKTV/live/aktv3/nhk/AKTV.m3u8
Nick Jr,http://aktv.top/AKTV/live/aktv3/nickjr/AKTV.m3u8
Nickelodeon,http://aktv.top/AKTV/live/aktv3/nickelodeon/AKTV.m3u8
Thrill,http://aktv.top/AKTV/live/aktv3/thrill/AKTV.m3u8
ZooMoo,http://aktv.top/AKTV/live/aktv3/zoomoo/AKTV.m3u8
亞洲新聞台,http://aktv.top/AKTV/live/aktv3/null-3/AKTV.m3u8
東方衛視國際頻道,http://aktv.top/AKTV/live/aktv3/null/AKTV.m3u8
無線衛星新聞台,http://aktv.top/AKTV/live/aktv3/null-2/AKTV.m3u8
翡翠台4K,http://aktv.top/AKTV/live/aktv3/4k/AKTV.m3u8


